package org.leadium.report.reportportal

import com.codeborne.selenide.Condition
import org.aspectj.lang.JoinPoint
import org.junit.jupiter.api.extension.*
import org.leadium.core.logevent.LogEventReceiver
import org.leadium.core.logevent.data.LogEventData
import org.leadium.core.report.StepReceiver
import org.leadium.core.report.reportportal.data.IssueTypeLocator
import org.leadium.core.report.reportportal.data.ReportPortalStepStatus
import org.leadium.core.report.reportportal.data.ReportPortalStepStatus.failed
import org.leadium.core.report.reportportal.data.ReportPortalStepStatus.passed
import org.leadium.core.utils.replaceCamelCaseWithSpaces
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import java.io.File

/**
 * @suppress
 */
class ReportAdapter : LogEventReceiver, StepReceiver,
    BeforeAllCallback,
    BeforeTestExecutionCallback,
    AfterTestExecutionCallback,
    AfterAllCallback {

    var launchUuid: String? = null
    var suiteUuid: String? = null
    var testUuid: String? = null

    //    var stepUuid: String? = null
    var openNestedSteps = ArrayList<String>()
    var reportPortalApiLifecycle: ReportPortalApiLifecycle? = null

    override fun onReceive(logEventData: LogEventData) {
        val action = logEventData.action.replaceCamelCaseWithSpaces().toLowerCase().replace("_", " ")
        val value = if (logEventData.parameter != null) {
            when (val parameter = logEventData.parameter) {
                is String -> ": $parameter"
                is Short -> " seconds: $parameter"
                is Int -> " index: $parameter"
                is File -> " file: ${parameter.path}"
                is Condition -> " $parameter"
                is Long -> " $parameter ms"
                is By -> " by: $parameter"
                is Array<*> -> {
                    val sb = StringBuilder()
                    sb.append(" ")
                    parameter.forEach {
                        when (it) {
                            is CharSequence -> sb.append(it)
                            is Int -> sb.append(it).append(" ")
                            is File -> sb.append(it.path).append(" ")
                            is Condition -> sb.append(it).append(" ")
                            is By -> sb.append(it).append(" ")
                            is Keys -> sb.append(it.name).append(" ")
                        }
                    }
                    sb.toString()
                }
                else -> ""
            }
        } else ""
        val result = if (logEventData.result != null) {
            when (val result = logEventData.result) {
                is String -> " result: $result"
                is Boolean -> " result: $result"
                is File -> " result: ${result.path}"
                else -> ""
            }
        } else ""
        val label = logEventData.label
        if (openNestedSteps.isNotEmpty())
            if (label != null) {
                reportPortalApiLifecycle!!.attachLogToTestItem(
                    launchUuid!!,
                    openNestedSteps.last(),
                    "'$label' $action$value$result",
                    "info"
                )
//                startNested("'$label' $action$value$result")
            } else {
                reportPortalApiLifecycle!!.attachLogToTestItem(
                    launchUuid!!,
                    openNestedSteps.last(),
                    "$action$value$result",
                    "info"
                )
//                startNested("$action$value$result")
            }
        else {
            if (testUuid != null) {
                val stepUuid = if (label != null) {
                    reportPortalApiLifecycle!!.attachLogToTestItem(
                        launchUuid!!,
                        testUuid!!,
                        "'$label' $action$value$result",
                        ""
                    )
                } else {
                    reportPortalApiLifecycle!!.attachLogToTestItem(launchUuid!!, testUuid!!, "$action$value$result", "")
                }
            }
        }
//        nestedStepUuid?.let {
//            reportPortalApiLifecycle!!.finishNestedStep(
//                launchUuid!!,
//                nestedStepUuid!!,
//                passed
//            )
//        }
    }

    private fun startNested(name: String): String {
        if (openNestedSteps.isNotEmpty())
            return reportPortalApiLifecycle!!.startNestedStep(launchUuid!!, openNestedSteps.last(), name)
        else {
            if (testUuid != null)
                return reportPortalApiLifecycle!!.startNestedStep(launchUuid!!, testUuid!!, name)
            return "Empty!"
        }
    }

    override fun beforeAll(context: ExtensionContext) {
        reportPortalApiLifecycle = ReportPortalApiLifecycle()
        launchUuid = reportPortalApiLifecycle!!.startLaunch("launch", "")
        suiteUuid = reportPortalApiLifecycle!!.startSuite(launchUuid!!, context.displayName, context.uniqueId)
    }

    override fun beforeTestExecution(context: ExtensionContext) {
        suiteUuid?.let {
            testUuid =
                reportPortalApiLifecycle!!.startTest(launchUuid!!, suiteUuid!!, context.displayName, context.uniqueId)
        }
    }

    override fun afterTestExecution(context: ExtensionContext?) {
        val failed = context!!.executionException.isPresent
        if (failed) {
            val message = context.executionException.get().localizedMessage
            val skipped = message!!.contains("Assumption failed")
            var status = ReportPortalStepStatus.failed
            if (skipped) status = ReportPortalStepStatus.skipped
            val stepUuid = reportPortalApiLifecycle!!.startStep(launchUuid!!, testUuid!!, message, "")
            reportPortalApiLifecycle!!.finishStep(launchUuid!!, stepUuid, status, IssueTypeLocator.PB001)
        }
        reportPortalApiLifecycle!!.finishTest(launchUuid!!, testUuid!!)
        testUuid = null
    }

    override fun afterAll(context: ExtensionContext?) {
        reportPortalApiLifecycle!!.finishSuite(launchUuid!!, suiteUuid!!)
        reportPortalApiLifecycle!!.finishLaunch(launchUuid!!)
        suiteUuid = null
        launchUuid = null
    }

    override fun before(joinPoint: JoinPoint?, uuid: String, name: String) {
        if (reportPortalApiLifecycle != null) {
            val nestedStepUuid = reportPortalApiLifecycle!!
                .startNestedStep(
                    launchUuid!!,
                    testUuid!!,
                    name
                )
            openNestedSteps.add(nestedStepUuid)
        }
    }

    override fun afterReturning(joinPoint: JoinPoint?, uuid: String) {
        if (reportPortalApiLifecycle != null)
            reportPortalApiLifecycle!!.finishNestedStep(
                launchUuid!!,
                openNestedSteps.last(),
                passed
            )
    }

    override fun afterThrowing(joinPoint: JoinPoint?, uuid: String, e: Throwable) {
        if (reportPortalApiLifecycle != null)
            reportPortalApiLifecycle!!.finishNestedStep(
                launchUuid!!,
                openNestedSteps.last(),
                failed
            )
    }

    override fun after(joinPoint: JoinPoint?, uuid: String) {
        openNestedSteps.removeAt(openNestedSteps.lastIndex)
    }
}