package org.leadium.core.report.reportportal.data

/**
 * @suppress
 */
enum class ReportPortalStepStatus {
    passed, failed, stopped, skipped, interrupted, cancelled
}