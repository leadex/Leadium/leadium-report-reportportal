package org.leadium.core.report.reportportal.data

/**
 * @suppress
 */
enum class LogLevel(val value: Int) {

    error(40000),
    warn(30000),
    info(20000),
    debug(10000),
    trace(5000),
    fatal(50000),
    unknown(60000)
}