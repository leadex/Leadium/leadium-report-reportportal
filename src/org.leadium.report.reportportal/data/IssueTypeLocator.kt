package org.leadium.core.report.reportportal.data

/**
 * @suppress
 */
enum class IssueTypeLocator {
    AB001, ND001, TI001, SI001, PB001
}