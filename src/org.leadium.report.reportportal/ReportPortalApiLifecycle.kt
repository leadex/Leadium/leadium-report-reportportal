package org.leadium.report.reportportal

import com.google.common.io.Files
import org.apache.log4j.Logger
import org.json.JSONObject
import org.leadium.core.report.reportportal.data.IssueTypeLocator
import org.leadium.core.report.reportportal.data.LogLevel
import org.leadium.core.report.reportportal.data.ReportPortalStepStatus
import org.leadium.core.utils.CurlExecutor
import java.io.File

/**
 * @suppress
 */
class ReportPortalApiLifecycle(val username: String = "superadmin", val password: String = "erebus") {

    private val log = Logger.getLogger(this.javaClass.simpleName)

    val localhost = "http://localhost:8080"
    val projectName = "SUPERADMIN_PERSONAL"
    val version = "v1"
    val curlExecutor = CurlExecutor()
    var apiAccessToken: String? = null

    init {
        val uiAccessToken = getUIAccessToken(username, password)
        apiAccessToken = getAPIAccessToken(uiAccessToken)
    }

    private fun getUIAccessToken(username: String, password: String): String {
        val url = "$localhost/uat/sso/oauth/token"
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/x-www-form-urlencoded",
            "--request", "POST",
            "--data", "grant_type=password&username=$username&password=$password",
            "--user", "ui:uiman", url
        )
        val response = curlExecutor.executeCommand(*command)
        val jsonObject = JSONObject(response)
        log.info("GET UI ACCESS:")
        log.info("endpoint: $url")
        log.info("response: $response")
        return jsonObject.getString("access_token")
    }

    private fun getAPIAccessToken(uiAccessToken: String): String {
        val url = "$localhost/uat/sso/me/apitoken"
        val command = arrayOf(
            "curl",
            "--header", "Authorization: Bearer $uiAccessToken",
            "--request", "GET", url
        )
        val response = curlExecutor.executeCommand(*command)
        val jsonObject = JSONObject(response)
        log.info("GET API ACCESS:")
        log.info("endpoint: $url")
        log.info("response: $response")
        return jsonObject.getString("access_token")
    }

    fun startLaunch(name: String, description: String): String {
        val startTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/launch"
        val json = """
                {
                  "name": "$name",
                  "description": "$description",
                  "start_time": "$startTime",
                  "mode": "DEFAULT",
                  "attributes": [
                    {
                      "key": "build",
                      "value": "0.1"
                    },
                    {
                      "value": "test"
                    }   
                  ] 
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("START LAUNCH:")
        log.info("endpoint: $url")
        log.info("response: $response")
        val jsonObject = JSONObject(response)
        return jsonObject.getString("id")
    }

    fun finishLaunch(launchUuid: String) {
        val endTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/launch/$launchUuid/finish"
        val json = """
                {
                  "end_time": "$endTime"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "PUT",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("FINISH LAUNCH:")
        log.info("endpoint: $url")
        log.info("response: $response")
    }

    fun startSuite(launchUuid: String, name: String, description: String): String {
        val startTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item"
        val json = """
                {
                  "name": "$name",
                  "start_time": "$startTime",
                  "type": "suite",
                  "launch_id": "$launchUuid",
                  "description": "$description"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("START SUITE:")
        log.info("endpoint: $url")
        log.info("response: $response")
        val jsonObject = JSONObject(response)
        return jsonObject.getString("id")
    }

    fun finishSuite(launchUuid: String, suiteUuid: String) {
        val endTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$suiteUuid"
        val json = """
                {
                  "end_time": "$endTime",
                  "launch_id": "$launchUuid"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "PUT",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("FINISH SUITE:")
        log.info("endpoint: $url")
        log.info("response: $response")
    }

    fun startTest(
        launchUuid: String,
        suiteUuid: String,
        name: String,
        description: String
    ): String {
        val startTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$suiteUuid"
        val json = """
                {
                  "name": "$name",
                  "start_time": "$startTime",
                  "type": "test",
                  "launch_id": "$launchUuid",
                  "description": "$description"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("START TEST:")
        log.info("endpoint: $url")
        log.info("response: $response")
        val jsonObject = JSONObject(response)
        return jsonObject.getString("id")
    }

    fun finishTest(launchUuid: String, testUuid: String) {
        val endTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$testUuid"
        val json = """
                {
                  "end_time": "$endTime",
                  "launch_id": "$launchUuid"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "PUT",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("FINISH TEST:")
        log.info("endpoint: $url")
        log.info("response: $response")
    }

    fun startStep(
        launchUuid: String,
        testUuid: String,
        name: String,
        description: String
    ): String {
        val startTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$testUuid"
        val json = """
                {
                  "name": "$name",
                  "start_time": "$startTime",
                  "type": "step",
                  "launch_id": "$launchUuid",
                  "description": "$description"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("START STEP:")
        log.info("endpoint: $url")
        log.info("response: $response")
        val jsonObject = JSONObject(response)
        return jsonObject.getString("id")
    }

    fun finishStep(
        launchUuid: String,
        stepUuid: String,
        status: ReportPortalStepStatus,
        issueTypeLocator: IssueTypeLocator,
        issueComment: String = ""
    ) {
        val endTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$stepUuid"
        val json = """
                {
                  "end_time": "$endTime",
                  "status": "${status.name}",
                  "launch_id": "$launchUuid",
                  "issue": {
                    "issue_type": "${issueTypeLocator.name}",
                    "comment": "$issueComment"    
                  }
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "PUT",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("FINISH STEP:")
        log.info("endpoint: $url")
        log.info("response: $response")
    }

    fun startNestedStep(launchUuid: String, stepUuid: String, name: String): String {
        val startTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$stepUuid"
        val json = """
                {
                  "name": "$name",
                  "start_time": "$startTime",
                  "type": "step",
                  "hasStats": false,
                  "launch_id": "$launchUuid"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("START NESTED STEP:")
        log.info("endpoint: $url")
        log.info("response: $response")
        val jsonObject = JSONObject(response)
        return jsonObject.getString("id")
    }

    fun finishNestedStep(
        launchUuid: String,
        nestedStepUuid: String,
        status: ReportPortalStepStatus
    ) {
        val endTime = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/item/$nestedStepUuid"
        val json = """
                {
                  "end_time": "$endTime",
                  "status": "${status.name}",
                  "launch_id": "$launchUuid"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "PUT",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("FINISH NESTED STEP:")
        log.info("endpoint: $url")
        log.info("response: $response")
    }

    fun attachLogToTestItem(
        launchUuid: String,
        itemUuid: String,
        message: String,
        level: String
    ) {
        val time = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/log"
        val json = """
                {
                  "launch_id": "$launchUuid",
                  "item_id": "$itemUuid",
                  "time": "$time",
                  "message": "$message",
                  "level": "$level"
                }
            """.trimIndent()
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: application/json",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--data", json, url
        )
        val response = curlExecutor.executeCommand(*command)
        log.info("ATTACH LOG:")
        log.info("endpoint: $url")
        log.info("response: $response")
    }

    fun attachFileToTestItem(
        launchUuid: String,
        itemUuid: String,
        file: File,
        message: String,
        level: LogLevel
    ) {
        val time = System.currentTimeMillis()
        val url = "$localhost/api/$version/$projectName/log"
        val json = """
                  [{
                    "itemUuid": "$itemUuid",
                	"launchUuid": "$launchUuid",
                	"time": "$time",
                	"message": "$message",
                	"level": ${level.value},
                	"file": {
                	  "name": "${file.name}"
                	}
                  }]
            """.trimIndent()
        val jsonFile = File("temp.json")
        Files.write(json.byteInputStream().readBytes(), jsonFile)
        val command = arrayOf(
            "curl",
            "--header", "Content-Type: multipart/form-data",
            "--header", "Authorization: Bearer $apiAccessToken",
            "--request", "POST",
            "--form", "json_request_part=@${jsonFile.absolutePath};type=application/json",
            "--form", "file=@${file.path}",
            url
        )

        val response = curlExecutor.executeCommand(*command)
        log.info("ATTACH FILE:")
        log.info("endpoint: $url")
        log.info("response: $response")
        jsonFile.delete()
    }
}