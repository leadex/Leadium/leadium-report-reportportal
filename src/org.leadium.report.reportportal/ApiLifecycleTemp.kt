package org.leadium.report.reportportal

import org.leadium.core.report.reportportal.data.IssueTypeLocator
import org.leadium.core.report.reportportal.data.LogLevel
import org.leadium.core.report.reportportal.data.ReportPortalStepStatus
import org.leadium.report.reportportal.ReportPortalApiLifecycle
import java.io.File

/**
 * @suppress
 */
object ApiLifecycleTemp {

    @JvmStatic
    fun main(args: Array<String>) {
        val file =
            File("/home/almayce/Projects/testing/bae/bpm-automation-testing/source/apps/fundstransfer/resources/fundstransfer/dev/mobile/clone.json")
        val file2 = File("/home/almayce/almayce/pics/coal2.jpg")

        val reportPortalApiLifecycle = ReportPortalApiLifecycle()

        val launchUuid = reportPortalApiLifecycle.startLaunch("funds_transfer_launch", "first curl launch")
        val suiteUuid = reportPortalApiLifecycle.startSuite(launchUuid, "new_suite", "any suite description")
        val testUuid = reportPortalApiLifecycle.startTest(launchUuid, suiteUuid, "new_container", "any container")
        val stepUuid = reportPortalApiLifecycle.startStep(launchUuid, testUuid, "new_step", "any step description")

        reportPortalApiLifecycle.attachLogToTestItem(launchUuid, stepUuid, "TEST MESSAGE", "error")

        reportPortalApiLifecycle.attachFileToTestItem(launchUuid, launchUuid, file, "some message", LogLevel.error)
        reportPortalApiLifecycle.attachFileToTestItem(launchUuid, stepUuid, file2, "some message", LogLevel.trace)

        val nestedStepUuid = reportPortalApiLifecycle.startNestedStep(launchUuid, stepUuid, "new_nested_step")
        reportPortalApiLifecycle.attachFileToTestItem(launchUuid, nestedStepUuid, file, "some message", LogLevel.debug)

        reportPortalApiLifecycle.attachLogToTestItem(launchUuid, nestedStepUuid, "TEST MESSAGE1", "error")
        reportPortalApiLifecycle.attachLogToTestItem(launchUuid, nestedStepUuid, "TEST MESSAGE2", "error")
        reportPortalApiLifecycle.attachLogToTestItem(launchUuid, nestedStepUuid, "TEST MESSAGE3", "error")
        reportPortalApiLifecycle.finishNestedStep(launchUuid, nestedStepUuid, ReportPortalStepStatus.passed)

        val nestedStepUuid2 = reportPortalApiLifecycle.startNestedStep(launchUuid, stepUuid, "new_nested_step")
        reportPortalApiLifecycle.finishNestedStep(launchUuid, nestedStepUuid2, ReportPortalStepStatus.failed)
        reportPortalApiLifecycle.finishStep(
            launchUuid,
            stepUuid,
            ReportPortalStepStatus.failed,
            IssueTypeLocator.AB001,
            "comment"
        )

        reportPortalApiLifecycle.finishTest(launchUuid, testUuid)
        reportPortalApiLifecycle.finishSuite(launchUuid, suiteUuid)
        reportPortalApiLifecycle.finishLaunch(launchUuid)
    }
}